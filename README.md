# Top-News-Articles

view here- https://sumityadav8181.github.io/Top-News-Articles/

Pulling the data from newsapi.org in the JSON format and then converting it into the material card (Google's material design) for better reading & viewing experience.

This is my first project using an API, 

Things I learned(you can too,if you are beginner)-

1- How to access an API with GET method
2- Understanding JSON format and how to access the desired part of it
3- Displaying JSON data into HTML or the format you like
4- Using a third-party CSS Framework based on material design.
